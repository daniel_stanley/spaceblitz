package com.example.game1;


import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ConfigurationInfo;
import android.media.MediaPlayer;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
//import android.os.SystemClock;
import android.view.Window;

public class MainActivity extends Activity 
{
	/** Hold a reference to our GLSurfaceView */
	private GLSurfaceView mGLSurfaceView;
	static MediaPlayer music;
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		GameRenderer.mainMenu=true;
		
		music=MediaPlayer.create(this, R.raw.music);
        music.setLooping(true);
        music.setVolume(.5f,.5f);
        //music.start();
		
		//GameRenderer.startTime=SystemClock.uptimeMillis();
		//GameRenderer.falling=true;
		GameRenderer.frozenTime=0;
		
		GameRenderer.initialize();
		
		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mGLSurfaceView = new Game1View(this);

		// Check if the system supports OpenGL ES 2.0.
		final ActivityManager activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
		final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

		if (supportsEs2) 
		{
			// Request an OpenGL ES 2.0 compatible context.
			mGLSurfaceView.setEGLContextClientVersion(2);

			// Set the renderer to our demo renderer, defined below.
			mGLSurfaceView.setRenderer(new GameRenderer(this));
		} 
		else 
		{
			// This is where you could create an OpenGL ES 1.x compatible
			// renderer if you wanted to support both ES 1 and ES 2.
			return;
		}

		setContentView(mGLSurfaceView);
	}

	@Override
	protected void onResume() 
	{
		// The activity must call the GL surface view's onResume() on activity onResume().
		super.onResume();
		mGLSurfaceView.onResume();
		//Star.initialize(GameRenderer.stars);
		GameRenderer.startTime2=0l;
		
		SharedPreferences sharedPref=this.getPreferences(Context.MODE_PRIVATE);
		GameRenderer.highScore=sharedPref.getInt("highscore", 0);
		 GameRenderer.musicMuted=sharedPref.getBoolean("musicMuted", GameRenderer.musicMuted);
		 GameRenderer.soundMuted=sharedPref.getBoolean("soundMuted", GameRenderer.soundMuted);
		 
		 
		if(sharedPref.getBoolean("paused",false)){
			//if you make it to here, there should be a saved game
			//defaults might be used if the current game was not lost when the app was closed ???
			GameRenderer.paused=true;
			GameRenderer.falling=false;
			

			
			 
			for(int i=1;i<5;i++){
				GameRenderer.blocks[i]=sharedPref.getInt("blocks"+i, GameRenderer.blocks[i]);
			}
			GameRenderer.frozenTime=sharedPref.getInt("frozenTime", GameRenderer.frozenTime);
			GameRenderer.score=sharedPref.getInt("score",GameRenderer.score);
			GameRenderer.astronautEnabled=sharedPref.getBoolean("astronautEnabled",GameRenderer.astronautEnabled);
			GameRenderer.astronautX=sharedPref.getInt("astronautX",GameRenderer.astronautX);
			GameRenderer.astronautY=sharedPref.getInt("astronautY",GameRenderer.astronautY);
			GameRenderer.startTime=sharedPref.getLong("startTime", GameRenderer.startTime);
			//GameRenderer.startTime2=sharedPref.getLong("startTime2",GameRenderer.startTime2);
			//GameRenderer.as=sharedPref.getInt("astronautY",GameRenderer.astronautY);
			GameRenderer.oldScore=sharedPref.getInt("oldScore",GameRenderer.oldScore);
			GameRenderer.PlayerPosition=sharedPref.getInt("PlayerPosition",GameRenderer.PlayerPosition);
		}
		
		
		new Sensors(this);
		
		if(!sharedPref.getBoolean("musicMuted", GameRenderer.musicMuted)){
			music.start();
		}
	}

	@Override
	protected void onPause() 
	{
		// The activity must call the GL surface view's onPause() on activity onPause().
		super.onPause();
		mGLSurfaceView.onPause();
		if(GameRenderer.falling){
			GameRenderer.paused=true;
			GameRenderer.falling=false;
			GameRenderer.frozenTime=GameRenderer.time2;
		}
		
		//save high score
		SharedPreferences sharedPref=this.getPreferences(Context.MODE_PRIVATE);
		SharedPreferences.Editor editor=sharedPref.edit();
		editor.putInt("highscore", GameRenderer.highScore);
		editor.putBoolean("musicMuted", GameRenderer.musicMuted);
		editor.putBoolean("soundMuted", GameRenderer.soundMuted);
		
		editor.putBoolean("paused",GameRenderer.paused);//this tells whether there is a saved game or not;
		if(GameRenderer.paused){
			for(int i=1;i<5;i++){
				editor.putInt("blocks"+i, GameRenderer.blocks[i]);
			}
			editor.putInt("frozenTime", GameRenderer.time2);
			editor.putInt("score",GameRenderer.score);
			editor.putBoolean("astronautEnabled",GameRenderer.astronautEnabled);
			editor.putInt("astronautX",GameRenderer.astronautX);
			editor.putInt("astronautY",GameRenderer.astronautY);
			editor.putLong("startTime", GameRenderer.startTime);
			//editor.putLong("startTime2",GameRenderer.startTime2);
			//editor.putInt("astronautY",GameRenderer.astronautY);
			editor.putInt("oldScore",GameRenderer.oldScore);
			editor.putInt("PlayerPosition",GameRenderer.PlayerPosition);
		}
		editor.commit();
		music.pause();
		
	}
	
	@Override
	public void onBackPressed(){
		if(!GameRenderer.mainMenu){
			if(GameRenderer.falling){
				GameRenderer.paused=true;
				GameRenderer.falling=false;
				GameRenderer.frozenTime=GameRenderer.time2;
			}
			GameRenderer.mainMenu=true;
		}else{
			if(GameRenderer.about){
				GameRenderer.about=false;
			}else if(GameRenderer.stargazing){
				GameRenderer.stargazing=false;
			}else{
				super.onBackPressed();
			}
		}
	}
}