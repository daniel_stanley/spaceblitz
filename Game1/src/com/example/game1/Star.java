package com.example.game1;

import android.util.Log;

public class Star {
	public static float offset;
	public float x;
	float initY;
	float speed;
	int startTime;
	float size;
	static int test=-1;
	
	public Star(boolean isNew, int time){
		x=(float) (Math.random()*2-1);
		if(isNew){
			Log.v("tag","isNew!");
			initY=(float) (3.5*Math.random());
		}else{
			initY=(float) (-Math.random());
		}
		startTime=time;
		speed=(float) (Math.random()*.001+.001);
		size=(float) (.1*Math.random()+.03);
	}
	
	public static void drawStars(Star[] array, int time, GameRenderer renderer){
		float top=GameRenderer.TopLocation;
		for(int i=0;i<array.length;i++){
			Star temp=array[i];
			float location=top-(temp.initY+temp.speed*(time-temp.startTime));
			if(location<-.1f){
				array[i]=new Star(false,time);
			}
			//float specificOffset=
			renderer.drawSquare(((array[i].x+offset*array[i].speed*500f+1f)%2f+2f)%2f-1f,location,array[i].size,array[i].size,5);
		}
	}

	public static void initialize(Star[] stars) {
		for(int i=0;i<stars.length;i++){
			stars[i]=new Star(true,0);
		}
	}
}
