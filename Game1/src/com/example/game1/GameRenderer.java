package com.example.game1;



import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
//import android.media.MediaPlayer;
import android.media.SoundPool;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView.Renderer;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.SystemClock;
//import android.util.Log;

import com.example.game1.R;



/**
 * This class implements our custom renderer. Note that the GL10 parameter passed in is unused for OpenGL ES 2.0
 * renderers -- the static class GLES20 is used instead.
 */
public class GameRenderer implements Renderer 
{
	public volatile static int PlayerPosition=1;
	static float TopLocation=0;
	static int[] blocks=new int[5];
	public static boolean falling;
	public static long startTime;
	public static long startTime2;
	public static int frozenTime;
	public static int score;
	static int astronautY=-1;
	static int astronautX=-1;
	static int oldScore=4;
	static boolean astronautEnabled;
	static Star[] stars=new Star[100];
	static int highScore;
	static boolean paused;
	static int time2;
	public static int test;
	public static boolean invincible;
	
	public static boolean mainMenu;
	public static boolean about;
	public static boolean stargazing;
	public static boolean soundMuted;
	public static boolean musicMuted;
	
	int boom1ID;
	int appearID;
	SoundPool sounds;
	
	Context MainActivityContext;
	/**
	 * Store the model matrix. This matrix is used to move models from object space (where each model can be thought
	 * of being located at the center of the universe) to world space.
	 */
	private float[] mModelMatrix = new float[16];

	/**
	 * Store the view matrix. This can be thought of as our camera. This matrix transforms world space to eye space;
	 * it positions things relative to our eye.
	 */
	private float[] mViewMatrix = new float[16];

	/** Store the projection matrix. This is used to project the scene onto a 2D viewport. */
	private float[] mProjectionMatrix = new float[16];
	
	/** Allocate storage for the final combined matrix. This will be passed into the shader program. */
	private float[] mMVPMatrix = new float[16];
	
	/** Store our model data in a float buffer. */
	private final FloatBuffer mSquareVertices;

	/** This will be used to pass in the transformation matrix. */
	private int mMVPMatrixHandle;
	
	/** This will be used to pass in model position information. */
	private int mPositionHandle;

	/** How many bytes per float. */
	private final int mBytesPerFloat = 4;
	
	/** How many elements per vertex. */
	private final int mStrideBytes = 3 * mBytesPerFloat; //was 7 *... when color was included
	
	/** Offset of the position data. */
	private final int mPositionOffset = 0;
	
	/** Size of the position data in elements. */
	private final int mPositionDataSize = 3;
	
	
	/** Size of the color data in elements. */
	private int[] mTextureDataHandles;		
	
	
	private int programHandleTex;
	private FloatBuffer mPlayAgainCoordinates;
	private FloatBuffer mPlayAgainTextureCoordinates;
	
	private int mTextureUniformHandle;
	private int mTexCoordinateHandle;
	
	
	/**
	 * Initialize the model data.
	 */
	public GameRenderer(Context MAContext)
	{	
		MainActivityContext=MAContext;

		// This Square from -1 -1 to 1 1
		final float[] SquareVerticesData = {
				// X, Y, Z, 
				// R, G, B, A
	            -0.25f, -0.25f, 0.0f, 
	            //0.0f, 0.0f, 1.0f, 1.0f,
	            
	            0.25f, -0.25f, 0.0f, 
	            //0.0f, 0.0f, 1.0f, 1.0f,
	            
	            0.25f, 0.25f, 0.0f, 
	            //1.0f, 1.0f, 0.0f, 1.0f,
	            
	            -0.25f, 0.25f, 0.0f, 
	            //1.0f, 1.0f, 0.0f, 1.0f
	            };
		
		
		final float[] PlayAgainCoordinateData={
				-0.5f,-.25f,0.0f,
				0.5f,-.25f,0.0f,
				0.5f,.25f,0.0f,
				-0.5f,.25f,0.0f};
		
		final float[] PlayAgainTextureCoordinates={
				0f,1f,
				1f,1f,
				1f,0f,
				0f,0f};
		
		
		
	
		
		// Initialize the buffers.
		mSquareVertices = ByteBuffer.allocateDirect(SquareVerticesData.length * mBytesPerFloat)
        .order(ByteOrder.nativeOrder()).asFloatBuffer();
		mSquareVertices.put(SquareVerticesData).position(0);
		
		mPlayAgainCoordinates=ByteBuffer.allocateDirect(PlayAgainCoordinateData.length * mBytesPerFloat)
				.order(ByteOrder.nativeOrder()).asFloatBuffer();
		mPlayAgainCoordinates.put(PlayAgainCoordinateData).position(0);
		
		mPlayAgainTextureCoordinates=ByteBuffer.allocateDirect(PlayAgainTextureCoordinates.length * mBytesPerFloat)
		        .order(ByteOrder.nativeOrder()).asFloatBuffer();
		mPlayAgainTextureCoordinates.put(PlayAgainTextureCoordinates).position(0);
		
		
		
	}
	
	public void onSurfaceCreated(GL10 glUnused, EGLConfig config) 
	{
		//startTime2=SystemClock.uptimeMillis();
		//handled in first onDrawFrame instead
		
		
		Star.initialize(stars);
		
		// Set the background clear color
		GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	
		// Position the eye behind the origin.
		final float eyeX = 0.0f;
		final float eyeY = 0.0f;
		final float eyeZ = 1.0001f;//frustrum starts 1 forward

		// We are looking toward the distance
		final float lookX = 0.0f;
		final float lookY = 0.0f;
		final float lookZ = -5.0f;

		// Set our up vector. This is where our head would be pointing were we holding the camera.
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		// Set the view matrix. This matrix can be said to represent the camera position.
		// NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
		// view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
		Matrix.setLookAtM(mViewMatrix, 0, eyeX, eyeY, eyeZ, lookX, lookY, lookZ, upX, upY, upZ);
		
		
		//String vertexShader=ShaderHelper.readTextFileFromRawResource(MainActivityContext, R.raw.per_pixel_vertex_shader);;
		
		
		final String vertexShaderTex=
				"uniform mat4 u_MVPMatrix;      \n"		// A constant representing the combined model/view/projection matrix.
				
				  + "attribute vec4 a_Position;     \n"		// Per-vertex position information we will pass in.
				  + "attribute vec2 a_TexCoordinate;        \n"		// Per-vertex texture coordinate information we will pass in. 					  
				  
				  + "varying vec2 v_TexCoordinate;          \n"		// This will be passed into the fragment shader.
				  
				  + "void main()                    \n"		// The entry point for our vertex shader.
				  + "{                              \n"
				  + "   v_TexCoordinate = a_TexCoordinate;          \n"		// Pass through the texture coordinate.
				  											// It will be interpolated across the triangle.
				  + "   gl_Position = u_MVPMatrix   \n" 	// gl_Position is a special variable used to store the final position.
				  + "               * a_Position;   \n"     // Multiply the vertex by the matrix to get the final point in 			                                            			 
				  + "}                              \n";    // normalized screen coordinates.
		
		
		final String fragmentShaderTex =
				"precision mediump float;       \n"		// Set the default precision to medium. We don't need as high of a 
														// precision in the fragment shader.				
			  + "uniform sampler2D u_Texture;   \n"     // The input texture.
			  + "varying vec2 v_TexCoordinate;          \n"		// This is the color from the vertex shader interpolated across the 
			  											// triangle per fragment.			  
			  + "void main()                    \n"		// The entry point for our fragment shader.
			  + "{                              \n"
			  + "   gl_FragColor = texture2D(u_Texture, v_TexCoordinate);     \n"		// Pass the color directly through the pipeline.		  
			  + "}                              \n";			
		
		final int vertexShaderHandleTex = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShaderTex);		
		final int fragmentShaderHandleTex = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShaderTex);		
		
        final String[] attributes={"a_Position","a_TexCoordinate"};
        programHandleTex=ShaderHelper.createAndLinkProgram(vertexShaderHandleTex,fragmentShaderHandleTex,attributes);
      //glBindAttribLocation
        
     // Load the texture
        int[] textures= {R.drawable.play_again,R.drawable.ship3,R.drawable.explosion2,R.drawable.astronaut,R.drawable.numbers4,
        				 R.drawable.star,R.drawable.paused,R.drawable.menu_buttons2,R.drawable.about5,R.drawable.check,
        				 R.drawable.logo3};
        mTextureDataHandles = loadTexture(MainActivityContext, textures);
        
        sounds=new SoundPool(3, 3, 0);//second argument should be SoundPool.STREAM_MUSIC I think, but it's not working
        boom1ID=sounds.load(MainActivityContext,R.raw.boom1,1);
        appearID=sounds.load(MainActivityContext,R.raw.appear,1);

        
        
        
        /**
     // Pass in the texture coordinate information
        mPlayAgainTextureCoordinates.position(0);
        int tempHandle=GLES20.glGetAttribLocation(programHandleTex, "a_TexCoordinate");
        //I'm (stupidly) hardcoding the coordinate data size 
        GLES20.glVertexAttribPointer(tempHandle, 32, GLES20.GL_FLOAT, false, 
        		0, mPlayAgainTextureCoordinates);
        GLES20.glEnableVertexAttribArray(tempHandle);
        **/
        
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA,GLES20.GL_ONE_MINUS_SRC_ALPHA);
	}	
	
	public void onSurfaceChanged(GL10 glUnused, int width, int height) 
	{
		// Set the OpenGL viewport to the same size as the surface.
		GLES20.glViewport(0, 0, width, height);

		// Create a new perspective projection matrix. The height will stay the same
		// while the width will vary as per aspect ratio.
		final float ratio = (float) height / width;
		final float left = -1;
		final float right = 1;
		final float bottom = 0f;
		final float top = 2.0f*ratio;
		final float near = 1.0f;
		final float far = 10.0f;
		
		TopLocation=top;
		
		Matrix.frustumM(mProjectionMatrix, 0, left, right, bottom, top, near, far);
	}	

	public void onDrawFrame(GL10 glUnused) 
	{
		if(startTime2==0){
			//should only happen the first time
			//need this because it is ~a second and a half from onSurfaceCreated to the first onDrawFrame
			startTime2=SystemClock.uptimeMillis();
		}
		
		GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);			        
                
        
		
		//int time2;
		if(!falling){
			time2=frozenTime;
			
		}else{
			time2=(int)(SystemClock.uptimeMillis()-startTime);
		}
		
		if(invincible && time2>500 && falling){
			//avoids dying within the first 1/2 second
			invincible=false;
			//Log.v("Debug","Debug, time is"+time2);
		}
		
		Star.drawStars(stars,(int)(SystemClock.uptimeMillis()-startTime2),this);
		
		
		
		
		
		
		
		if(mainMenu){
			if(about){
				drawSquare(0f,TopLocation/2.0f,4f,TopLocation*2.0f,8);
				if(soundMuted){
					drawSquare(.736f,.495f*TopLocation,.25f,.25f,9);
				}
				if(musicMuted){
					drawSquare(.736f,.395f*TopLocation,.25f,.25f,9);
				}
			}else if(stargazing){
				//don't draw anything
			}else{
				drawSquare(0f,TopLocation-.7f,3.1f,3.1f,10);
				drawSquare(0f,1f,3.2f,3.2f,7);
			}
			
			
		}else{

			drawSquare(-0.75f+0.5f*PlayerPosition,.25f,1,1,1);

			float speed=0.003f+.00000001f*time2;
			
			
			for(int i=0;i<5;i++){
				float location;
				if(time2<(int)(10.0f/speed)){
					location=17-((i*2.0f+time2*speed));
				}else{
					location=7-((i*2.0f+time2*speed)%10.0f);
				}


				if(location<TopLocation+5 && location>-1.0f){
					//block is on the screen (or first frame off)
					drawSquare(-0.75f+0.5f*blocks[i],location,1f,1f,2);
					if(blocks[i]==PlayerPosition && location>-0.2f && location<0.75f && !invincible){
						//lose

						//only on the frame you lose, 
						if(!soundMuted){
							sounds.play(boom1ID, 1f, 1f, 2, 0, 1f);
						}
						//Log.v("debug","Debug, Playing Sound!");
						frozenTime=time2;
						if(score>highScore){
							highScore=score;
						}
						GameRenderer.invincible=true;
						time2=0;
						falling=false;
					}


					if(location<=-0.25f && ((-oldScore)%5+5)%5==i){//the mod thing is a rather dirty way of only performing this once on each block
						//just off the bottom
						oldScore++;
						blocks[i]=(int) (Math.random()*4);
						//System.out.println("i is now "+i);
						if(astronautY==-1 && Math.random()<.5){
							astronautY=i;
							astronautX=(int)(4*Math.random());
						}
					}

					//now for the astronaut

					if(astronautY==i && location>TopLocation+.25f){
						astronautEnabled=true;
					}

					if(astronautEnabled && astronautY==i){
						location-=1;
						drawSquare(-0.75f+0.5f*astronautX,location,1f,1f,3);
						if(location<TopLocation+1 && location>-1.0f){
							drawSquare(-0.75f+0.5f*astronautX,location,1f,1f,3);
						}
						if(astronautX==PlayerPosition && location>-0.25f && location<0.75f){
							if(!soundMuted){
								sounds.play(appearID,  1f,  1f,  1,  0,  2f);
							}
							score++;
							astronautY=-1;
						}
						if(location<=-0.25f){
							astronautY=-1;
							astronautEnabled=false;
						}
					}
				}
			}
			drawInt(.9f,TopLocation-.2f,.2f,.2f,score);
			drawInt(-.75f,TopLocation-.2f,.2f,.2f,highScore);


			if(!falling){
				if(paused){
					drawSquare(0f,2.5f,2f,1f,6);
				}else{
					drawSquare(0f,2.5f,2f,1f,0);
				}
			}


		}
	}
	
	/**
	 * Draws a triangle from the given vertex data.
	 * 
	 * @param aTriangleBuffer The buffer containing the vertex data.
	 */
	
	
	private void drawInt(float x, float y, float width, float height, int number){
		if(number==0){
			drawDigit(x,y,width,height,0);
		}else{

			//coordinates map to center of rightmost digit
			int length=(int) Math.log10(number)+1;
			//int[] digits=new int[length];
			//move right to left
			for(int i=0;i<length;i++){
				drawDigit(x-i*width*.5f,y,width,height,(int) ((number%Math.pow(10, i+1))/Math.pow(10, i)) );
			}
		}
	}
	
	
	private void drawDigit(float x, float y, float width, float height, int digit){
		float[] texData={
				0.1f*digit,1f,
				0.1f*digit+0.1f,1f,
				0.1f*digit+0.1f,0f,
				0.1f*digit,0f};
		FloatBuffer mTexData=ByteBuffer.allocateDirect(texData.length * mBytesPerFloat)
		        .order(ByteOrder.nativeOrder()).asFloatBuffer();
		mTexData.put(texData).position(0);
		
		
		
		
		
		mPositionHandle = GLES20.glGetAttribLocation(programHandleTex, "a_Position");
        mTexCoordinateHandle = GLES20.glGetAttribLocation(programHandleTex, "a_TexCoordinate");
        
        mTextureUniformHandle = GLES20.glGetUniformLocation(programHandleTex, "u_Texture");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(programHandleTex, "u_MVPMatrix");
        
        
        GLES20.glUseProgram(programHandleTex);        
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandles[4]);
        GLES20.glUniform1i(mTextureUniformHandle, 0);        
		
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.scaleM(mModelMatrix,0,width,height,1f);
        Matrix.translateM(mModelMatrix, 0, x/width, y/height, 0.0f);
        
        mSquareVertices.position(mPositionOffset);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
        		mStrideBytes, mSquareVertices);
        GLES20.glEnableVertexAttribArray(mPositionHandle);      
        
        mSquareVertices.position(mPositionOffset);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mTexCoordinateHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
        		8, mTexData);
        GLES20.glEnableVertexAttribArray(mTexCoordinateHandle); 
        
        
        Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
        
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
	}
	
	
	void drawSquare(float x, float y, float width, float height, int textureNumber)
	{   
		mPositionHandle = GLES20.glGetAttribLocation(programHandleTex, "a_Position");
        mTexCoordinateHandle = GLES20.glGetAttribLocation(programHandleTex, "a_TexCoordinate");
        
        mTextureUniformHandle = GLES20.glGetUniformLocation(programHandleTex, "u_Texture");
        mMVPMatrixHandle = GLES20.glGetUniformLocation(programHandleTex, "u_MVPMatrix");
        
        
        // Tell OpenGL to use this program when rendering.
        GLES20.glUseProgram(programHandleTex);        
		
		//mTextureUniformHandle = GLES20.glGetUniformLocation(programHandleTex, "u_Texture");
		//mTextureCoordinateHandle = GLES20.glGetAttribLocation(programHandleTex, "a_TexCoordinate");
		
		// Set the active texture unit to texture unit 0.
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        // Bind the texture to this unit.
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureDataHandles[textureNumber]);
        // Tell the texture uniform sampler to use this texture in the shader by binding to texture unit 0.
        GLES20.glUniform1i(mTextureUniformHandle, 0);        
		
		
		
		Matrix.setIdentityM(mModelMatrix, 0);
		Matrix.scaleM(mModelMatrix,0,width,height,1f);
        Matrix.translateM(mModelMatrix, 0, x/width, y/height, 0.0f);
        
		
		
        //index, a handle found with glGetAttribLocation
        //size (components per attribute?)
        //type
        //normalized (? t/f)
        //stride (byte offset between consecutive generic vertex attributes)
        //pointer (for the first component?)
		// Pass in the position information
        
        
        /**
         * the following couple lines link a_position in the shader to mSquareVertices
         * Remember mSquare vertices might have more info than just the position we want, like color
         * set the buffer to the position of the first piece we want
         * mPosition handle was found using glGetAttribLocation after compiling the shader
         * mPositionDataSize is the size of the data bits we want
         * ---(even if position and color data are in the same array, position data points are 3 elements long)
         * the constant is the data type in the array, in this case floats
         * Whether or not it is normalized (not sure what this is actually)
         * Stride bytes are how many bytes in the array between sets of data we want
         * ---(if there were also color in the array, stride bytes would be the size of position+color data)
         * ---(if the array only holds the stuff you want, mPositionOffset=0 and mPositionDataSize*bytesPerObject=mStrideBytes)
         * finally, pass the array thing you wanted to link to
         * Don't forget to enable it (whatever that means)
         **/
        
        mSquareVertices.position(mPositionOffset);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mPositionHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
        		mStrideBytes, mSquareVertices);
        GLES20.glEnableVertexAttribArray(mPositionHandle);      
        
        mSquareVertices.position(mPositionOffset);//mPositionOffset=0
        GLES20.glVertexAttribPointer(mTexCoordinateHandle, mPositionDataSize, GLES20.GL_FLOAT, false,
        		8, mPlayAgainTextureCoordinates);
        GLES20.glEnableVertexAttribArray(mTexCoordinateHandle); 
        
        /**
        // Pass in the color information
        mSquareVertices.position(mColorOffset);
        GLES20.glVertexAttribPointer(mColorHandle, mColorDataSize, GLES20.GL_FLOAT, false,
        		mStrideBytes, mSquareVertices);  
        GLES20.glEnableVertexAttribArray(mColorHandle);
        **/
		// This multiplies the view matrix by the model matrix, and stores the result in the MVP matrix
        // (which currently contains model * view).
        Matrix.multiplyMM(mMVPMatrix, 0, mViewMatrix, 0, mModelMatrix, 0);
        
        // This multiplies the modelview matrix by the projection matrix, and stores the result in the MVP matrix
        // (which now contains model * view * projection).
        Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mMVPMatrix, 0);

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mMVPMatrix, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 4);
	}
	
	public static void move(int direction){
		int newPos=PlayerPosition+direction;
		if(newPos>=0&&newPos<=3){
			PlayerPosition=newPos;
		}
	}

	public static void initialize() {
		for(int i=0;i<5;i++){
			blocks[i]=(int)(4*Math.random());
		}
	}

	public static int[] loadTexture(final Context context, final int[] resourceId)
	{
		int numTextures=resourceId.length;
		final int[] textureHandle = new int[numTextures];
		GLES20.glGenTextures(numTextures, textureHandle, 0);

		for(int i=0;i<numTextures;i++){

			if (textureHandle[i] != 0)
		
			{
				final BitmapFactory.Options options = new BitmapFactory.Options();
				//options.inScaled = false;	// No pre-scaling

				// Read in the resource
				final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), resourceId[i], options);

				// Bind to the texture in OpenGL
				GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureHandle[i]);

				// Set filtering
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
				GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

				// Load the bitmap into the bound texture.
				GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);

				// Recycle the bitmap, since its data has been loaded into OpenGL.
				bitmap.recycle();						
			}

			if (textureHandle[i] == 0)
			{
				throw new RuntimeException("Error loading texture number "+i);
			}
		}
		
		return textureHandle;
	}
}