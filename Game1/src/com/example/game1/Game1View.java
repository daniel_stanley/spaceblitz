package com.example.game1;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class Game1View extends GLSurfaceView 
{	
	//Context context;
	
        	//test comment 2
	public Game1View(Context context) 
	{
		super(context);		
	}
	
	public Game1View(Context context1, AttributeSet attrs) 
	{
		super(context1, attrs);	
		//context=context1;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		if (event != null)
		{			
			
			
			if (event.getAction() == MotionEvent.ACTION_DOWN)
			{
				float x = event.getX();
				float y = event.getY();


				if(GameRenderer.mainMenu){
					if(GameRenderer.about){
						if(16.0/20.0*getWidth()<x && x<19.0/20.0*getWidth()){
							if(9.0/20.0*getHeight()<y && y<13.0/20.0*getHeight()){
								if(y>11.0/20.0*getHeight()){
									GameRenderer.musicMuted^=true;
									if(GameRenderer.musicMuted){
										MainActivity.music.pause();
									}else{
										MainActivity.music.start();
									}
								}else{
									GameRenderer.soundMuted^=true;
									
								}
							}else{
								GameRenderer.about=false;
							}
						}else{
							GameRenderer.about=false;
						}
					}else if(GameRenderer.stargazing){
						GameRenderer.stargazing=false;
					}else{
						
						if(x<9.0*getWidth()/10.0 && x>getWidth()/10.0){
							double top=getHeight()-.1*getWidth();
							double bottom=getHeight()-.9*getWidth();
							if(y<top && y>bottom){//top is actually the one near the bottom of the phone
								if(y<bottom+(top-bottom)/3.0){
									
									if(!GameRenderer.paused){
										GameRenderer.startTime=SystemClock.uptimeMillis();
										GameRenderer.score=0;
										GameRenderer.falling=true;
										GameRenderer.time2=0;
									}
									GameRenderer.mainMenu=false;
								}else if(y>bottom+2.0*(top-bottom)/3.0){
									GameRenderer.stargazing=true;
								}else{
									GameRenderer.about=true;
								}
							}
						}
						
					}
					
					
				}else{

					if (GameRenderer.falling){
						if(y<getHeight()/4.0){
							GameRenderer.paused=true;
							GameRenderer.falling=false;
							GameRenderer.frozenTime=GameRenderer.time2;
						}else{
							if(x>this.getWidth()/2){
								GameRenderer.move(1);
							}else{
								GameRenderer.move(-1);
							}
						}
					}else{
						if(GameRenderer.paused && y>getWidth()/4){
							Star.test*=-1;
						}
						if(x>=getWidth()/4 && x<3*getWidth()/4 && y>2*getHeight()/8 && y<3*getHeight()/8){

							if(GameRenderer.paused){
								//unpause
								GameRenderer.paused=false;
								GameRenderer.startTime+=SystemClock.uptimeMillis()-GameRenderer.startTime-GameRenderer.frozenTime;
							}else{
								//play again

								GameRenderer.startTime=SystemClock.uptimeMillis();
								GameRenderer.score=0;

							}

							GameRenderer.falling=true;

						}
					}
				}	
			}
			
			return true;
		}
		else
		{
			return super.onTouchEvent(event);
		}		
	}

	// Hides superclass method.
	public void setRenderer(GameRenderer renderer, float density) 
	{
		super.setRenderer(renderer);
	}
}