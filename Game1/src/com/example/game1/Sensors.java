package com.example.game1;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class Sensors implements SensorEventListener{
	private SensorManager mSensorManager;
	private Sensor gyroscope;
	private long timestamp;
	
	public Sensors(Context mContext){
		mSensorManager=(SensorManager)(mContext.getSystemService(Context.SENSOR_SERVICE));
		gyroscope=mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mSensorManager.registerListener(this,gyroscope,SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void initialize(Context mContext){
		mSensorManager=(SensorManager)(mContext.getSystemService(Context.SENSOR_SERVICE));
		gyroscope=mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		mSensorManager.registerListener(this,gyroscope,SensorManager.SENSOR_DELAY_GAME);
	}
	
	public void onSensorChanged(SensorEvent event){
		
		if(timestamp!=0){
			final float dt=(event.timestamp-timestamp)*.000000001f;
			
					
			Star.offset+=event.values[1]*dt;
			
			Log.v("debug","Offset: : "+Star.offset);
			GameRenderer.test=(int) (dt*Math.pow(10, 9)*1000000);
		}
		timestamp=event.timestamp;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}
	
}
